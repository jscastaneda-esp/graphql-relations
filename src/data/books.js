export const books = [
  {
    id: "1",
    title: "Robot Dreams",
    author: "1",
    image: "https://m.media-amazon.com/images/I/51KLcakd21L.jpg"
  },
  {
    id: "2",
    title: "The Starts, Like Dust",
    author: "1",
    image: "https://images-na.ssl-images-amazon.com/images/I/915-4oZVpRL.jpg"
  },
  {
    id: "3",
    title: "Digital Fortress",
    author: "2",
    image: "https://m.media-amazon.com/images/I/416RZl-Q4jS.jpg"
  },
  {
    id: "4",
    title: "Thre Three Mosketeers",
    author: "3",
    image: "https://m.media-amazon.com/images/I/51ewNXKhOzL.jpg"
  },
];
